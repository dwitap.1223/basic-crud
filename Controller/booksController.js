import fs from "fs";

const dataBase = JSON.parse(fs.readFileSync("./Database/db.json"));
const dbBook = dataBase.books;

const bookController = {
  getAllBook: (req, res) => {
    try {
      res.status(200).json({
        message: "Get All Book",
        data: dbBook,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  getBookById: (req, res) => {
    try {
      for (let book of dbBook) {
        if (book.id == req.params.id) {
          return res.status(200).json({
            message: "Get Book by ID",
            data: book,
          });
        }
      }
      return res.status(400).json({
        message: `Book with id ${req.params.id} not found`,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  createNewExpense: (req, res) => {
    try {
      const newId = dbBook[dbBook.length - 1].id + 1;

      dbBook.push({
        id: newId,
        ...req.body,
      });

      fs.writeFileSync("./Database/db.json", JSON.stringify({ books: dbBook }));

      res.status(200).json({
        message: "Book added!",
        data: dbBook,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  editBookById: (req, res) => {
    try {
      const { id } = req.params;

      for (let i = 0; i < dbBook.length; i++) {
        if (dbBook[i].id == id) {
          dbBook[i] = {
            ...dbBook[i],
            ...req.body,
          };
        }
      }

      fs.writeFileSync("./Database/db.json", JSON.stringify({ books: dbBook }));

      return res.status(200).json({
        message: "Book edited",
        data: dbBook,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  deleteBookById: (req, res) => {
    try {
      const { id } = req.params;

      for (let i = 0; i < dbBook.length; i++) {
        if (dbBook[i].id == id) {
          dbBook.splice(i, 1);
        }
      }

      fs.writeFileSync("./Database/db.json", JSON.stringify({ books: dbBook }));

      return res.status(200).json({
        message: "Book deleted",
        data: dbBook,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
};

export default bookController;
