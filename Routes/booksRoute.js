import express from "express";
import bookController from "../Controller/booksController.js";

const router = express.Router();

router.get("/", bookController.getAllBook);
router.get("/:id", bookController.getBookById);
router.post("/", bookController.createNewExpense);
router.patch("/:id", bookController.editBookById);
router.delete("/:id", bookController.deleteBookById);

export default router;
