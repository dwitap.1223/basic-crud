import express from "express";

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

import booksRoute from "./Routes/booksRoute.js";

app.use("/book", booksRoute);

app.all("*", (req, res) => {
  res.status(404).json({
    message: "Api not found",
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
